package com.jiat.web;

import com.jiat.web.db.MySQL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@WebServlet(name = "Registeruser", urlPatterns = {"/registeruser"})

public class registeruser extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String tp = request.getParameter("tp");
        String email = request.getParameter("em");
        Connection connection = null;

        if (name.isEmpty()) {
            response.getWriter().write("Please enter Name");
        }else if (tp.isEmpty()) {
            response.getWriter().write("Please Enter telephone number");
        } else if (email.isEmpty()) {
            response.getWriter().write("Please Enter Your email");
        }else {

            try {

                if (connection!=null){

                    ResultSet r = MySQL.search("SELECT * FROM `users` WHERE name='"+name+"' OR email='"+email+"' OR `mobile`='"+tp+"'");
                    if (r.next()){
                        response.getWriter().write("User Already exists");
                    }else {
                        MySQL.iud("INSERT INTO users(`name`,`mobile`,`email`)VALUES('"+name+"','"+tp+"','"+email+"')");
                        response.sendRedirect("index.jsp");
                    }

                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }
}
