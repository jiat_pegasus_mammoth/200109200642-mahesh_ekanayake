package com.jiat.web.db;

import com.jiat.web.util.ApplicationProperties;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


public class MySQL {


    private static Connection connection;

    private static Statement createConnection() throws Exception {
        if (connection == null) {
            ApplicationProperties properties = ApplicationProperties.getInstance();
            Class.forName(properties.get("sql.connection.driver"));
            connection = DriverManager.getConnection(properties.get("sql.connection.url"), properties.get("sql.connection.username"), properties.get("sql.connection.password"));
        }
        return connection.createStatement();
    }

    public static void iud(String query) {
        try {
            createConnection().executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static ResultSet search(String query) throws Exception{
        return createConnection().executeQuery(query);
    }
    public static Connection getConnection() {
        try {
            ApplicationProperties properties = ApplicationProperties.getInstance();
            Class.forName(properties.get("sql.connection.driver"));
            connection = DriverManager.getConnection(properties.get("sql.connection.url"), properties.get("sql.connection.username"), properties.get("sql.connection.password"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

}

