package com.jiat.web;

import com.jiat.web.db.MySQL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@WebServlet(name = "updateuser", urlPatterns = {"/updateuser"})

public class updateuser extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = request.getParameter("nm");
        String id = request.getParameter("userid");

        String tp = request.getParameter("tele");

        Connection connection = null;

        if (name.isEmpty()) {
            response.getWriter().write("Please enter Name");
        } else if (tp.isEmpty()) {
            response.getWriter().write("Please Enter telephone number");
        } else {

            try {
                MySQL.iud("UPDATE `users` SET `name`='" + name + "',`mobile`='" + tp + "' WHERE `id`='" + id + "'");
                response.sendRedirect("index.jsp");
            } catch (Exception e) {
                throw new RuntimeException(e);
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }
}
