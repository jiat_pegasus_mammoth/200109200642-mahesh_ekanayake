package com.jiat.web;

import com.jiat.web.db.MySQL;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@WebServlet(name = "test", urlPatterns = "/test")
public class Test extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Connection connection = null;

        try {
            System.out.println("Yes--------------------");
            connection = MySQL.getConnection();

            System.out.println("----------------------------" + connection);
            Statement s = connection.createStatement();
            ResultSet r = s.executeQuery("SELECT * FROM `users`");
            while (r.next()){
               resp.getWriter().write(r.getString("name"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}