<%@ page import="java.util.ArrayList" %>
<%@ page import="com.jiat.web.db.MySQL" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %><%--
  Created by IntelliJ IDEA.
  User: Mahesh
  Date: 3/29/2023
  Time: 11:00 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Project_01</title>
    <style>
        #customers {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #customers tr:hover {
            background-color: #ddd;
        }

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #04AA6D;
            color: white;
        }

        button {
            background-color: #4CAF50; /* Green */
            border: none;

            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }


        .button2 {
            background-color: #f44336;
        }

        /* Red */
    </style>
</head>
<body>
<h1 style="text-align: center">Users</h1>
<hr style="border: 1px solid #160e0e">

<br>
<br>
<table id="customers">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Mobile</th>
        <th>Email</th>
        <th></th>
    </tr>

    <%
            ResultSet r = MySQL.search("SELECT * FROM `users`");
            while (r.next()) {
    %>
    <tr onclick="document.getElementById('name').value='<%=r.getString("name")%>',
            document.getElementById('tele').value='<%=r.getString("mobile")%>',
            document.getElementById('userid').value='<%=r.getString("id")%>'">


    <td><%=r.getString("id")%>
    </td>
    <td><%=r.getString("name")%>
    </td>
    <td><%=r.getString("mobile")%>
    </td>
    <td><%=r.getString("email")%>
    </td>
    <%--        <td><button class="button2" id="<%=r.getString("id")%>">Delete</button>--%>
    <td>
        <form action="deleteuser" action="POST">
            <input type="hidden" name="delete" value="<%=r.getString("id")%>"/>
            <button class="button2" type="submit" value="<%=r.getString("id")%>">Delete User</button>
        </form>

<%--            <button class="button"><a href="<%=response.encodeURL("userUpdate.jsp")%>">Update User</a></button>--%>

    </td>
    </tr>
    <%
            }

    %>


</table>


<hr style="border: 1px solid #160e0e">
<h1>Register User</h1>
<form action="registeruser" method="post">
    <table>
        <tr>
            <th>Name</th>
            <td><input type="text" name="name"/></td>
        </tr>

        <tr>
            <th>phone number</th>
            <td><input  type="text" name="tp"/></td>
        </tr>

        <tr>
            <th>Email</th>
            <td><input type="email" name="em"/></td>

        </tr>

        <tr>
            <td></td>
            <th colspan="2">

                <button type="submit" value="send">Save</button>
                <button class="button2" type="reset">Clear</button>
            </th>
        </tr>

    </table>
</form>

<h1>Update User Details</h1>
<hr style="border: 1px solid #160e0e">
<form action="updateuser" method="post">


    <table>
        <tr>
            <th>Name</th>
            <input type="hidden" id="userid" name="userid"/>
            <td><input id="name" type="text" name="nm"/></td>
        </tr>

        <tr>
            <th>phone number</th>
            <td><input id="tele" type="text" name="tele"/></td>
        </tr>



        <tr>
            <td></td>
            <th colspan="2">

                <button type="submit" value="send">Update</button>
                <button class="button2" type="reset">Clear</button>
            </th>
        </tr>

    </table>
</form>


</body>
</html>
